1. Объект проверки.

   Наборы тестов PetTest и UserTest проверяют функциональность api "Swagger Petstore" (https://petstore.swagger.io/).

   Проводится проверка работы различных endpoint-ов в сооветствующих разделах (pet,user).


2. В проект используются следующие библиотеки:

   MAVEN

   TESTNG

   JACKSON

   REST ASSURED

   LOMBOK

   FAKER

   ALLURE

   ASPECTJ

   ALLURE PLUGIN

   MAVEN SUREFIRE PLUGIN

   GITLAB-CI


3. Запуск проекта.

   Запуск тестового сценария осуществляется командой 'mvn clean test'.

   Для формирования Allure Report необходимо вызвать команду 'mvn allure:report'.

   Для запуска полученного Allure Report в веб сервисе небходимо вызвать команду 'mvn allure:serve'.

   Возможен опциональный запуск групповых тестов (get,put,post,delete) через GitLab Pipeline. А также супер теста раздела User (UserSuperTest) для наглядного применени @Step аннотаций.


4. Ожидаемые падения тестов.

   В наборе тестов UserTest ожидаемо падать могут тесты по добавлению или удалению юзера (а так же связанные с ними тесты по проверке присутствия/отсутствия этого юзера). Возможная причина - ограничение на стороне апи в виде непонятного условия эндпоинтов: This can only be done by the logged in user. 

   При этом обособленный запуск класса UserTest в 95% случаев проходит без падений. 

   Также, ожидаемо падает test_job gitlab pipeline в связи с наличием падающих тестов в проекте. Так, например, при запуске групповых пост тестов (postTest.testNg.xml) падение вызывает тест addNewPetImageTest, т.к. pipeline не может найти необходимый файл, даже при указании pathFromRepositoryRoot (а url не принимает эндпоинт апи).

