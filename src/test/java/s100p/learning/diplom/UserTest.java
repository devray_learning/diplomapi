package s100p.learning.diplom;

import io.qameta.allure.Description;
import io.restassured.RestAssured;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import s100p.learning.diplom.pogos.user.User;
import s100p.learning.diplom.pogos.user.UserError;
import s100p.learning.diplom.supportive.DataGenerationUtilities;

import java.util.List;

public class UserTest extends BaseTest implements DataGenerationUtilities {

    @DataProvider(name = "Names")
    public static Object[][] dataBB() {
        return new Object[][]{{"Neo"}, {"Frodo"}};
    }

    @Description("#1 Проверка функционала добавления нового юзера с указанием юзернейма")
    @Test(priority = 1, groups = "postTest", dataProvider = "Names")
    public void addNewUserTestWithNameTest(String username) {

        RestAssured
                .given()
                .spec(requestSpecification())
                .body(getNewRandomUserWithUserName(username))
                .when()
                .post("/user")
                .then()
                .spec(responseSpecification());
    }

    @Description("#2 Проверка функционала добавления нового юзера с указанием юзернейма и пароля")
    @Test(priority = 2, groups = "postTest")
    public void addNewUserWithNameAndPasswordTest() {

        RestAssured
                .given()
                .spec(requestSpecification())
                .body(getNewRandomUserWithUserNameAndPassword("Apple", "windows"))
                .when()
                .post("/user")
                .then()
                .spec(responseSpecification());
    }

    @Description("#3 Проверка базовых параметров: statusCode, contentType. На запрос по username")
    @Test(priority = 3, groups = "basicTest", dataProvider = "Names")
    public void basicDataTestWithDP(String username) {
        super.basicTest("/user", "https://petstore.swagger.io/v2/user/{username}", username, 200, "application/json");
    }

    @Description("#3.1 Проверка базовых параметров: statusCode, contentType. На запрос по username")
    @Test(priority = 3, groups = "basicTest")
    public void basicDataTest() {
        super.basicTest("/user", "https://petstore.swagger.io/v2/user/{username}", "Apple", 200, "application/json");
    }

    @Description("№4 Проверка получения тела объекта по заданному username")
    @Test(priority = 4, groups = "getTest", dataProvider = "Names")
    public void getUserByNameTestWithDP(String username) {
        RestAssured
                .given()
                .spec(requestSpecification())
                .when()
                .get("/user/{username}", username)
                .then()
                .log().body()
                .spec(responseSpecification())
                .extract().as(User.class);
    }

    @Description("№4.1 Проверка получения тела объекта по заданному username")
    @Test(priority = 4, groups = "getTest")
    public void getUserByNameTest() {
        RestAssured
                .given()
                .spec(requestSpecification())
                .when()
                .get("/user/{username}", "Apple")
                .then()
                .log().body()
                .spec(responseSpecification())
                .extract().as(User.class);
    }

    @Description("№5 Проверка функционала апдейта данных юзера")
    @Test(priority = 5, groups = "putTest", dataProvider = "Names")
    public void upDateUserTest(String username) {
        RestAssured
                .given()
                .spec(requestSpecification())
                //поскольку я указываю тот же username при апдейте объекта, то фактически идет апдейт всех остальных полей, т.к. faker генерит для них новые значения
                .body(getNewRandomUserWithUserName(username))
                .when()
                .put("/user/{username}", username)
                .then()
                .spec(responseSpecification());
    }

    @Description("#6 Проверка функционала добавления списка юзеров")
    @Test(priority = 6, groups = "postTest")
    public void addNewUserListTest() {
        List<User> userList = List.of(getNewRandomUser(), getNewRandomUser(), getNewRandomUser());

        RestAssured
                .given()
                .spec(requestSpecification())
                .body(userList)
                .when()
                .post("/user/createWithList")
                .then()
                .spec(responseSpecification());

        Assert.assertListNotContains(userList, user -> user.getPassword().isEmpty(), "");
    }

    @Description("№7 Проверка функционала login")
    @Test(priority = 7, groups = "getTest")
    public void userLogInTest() {
        RestAssured
                .given()
                .spec(requestSpecification())
                .queryParam("username", "Apple")
                .queryParam("password", "windows")
                .when()
                .get("/user/login")
                .then()
                .log().body()
                .spec(responseSpecification());
    }

    @Description("№8 Проверка функционала logout")
    @Test(priority = 8, groups = "getTest")
    public void userLogOutTest() {
        RestAssured
                .given()
                .spec(requestSpecification())
                .when()
                .get("/user/logout")
                .then()
                .log().body()
                .spec(responseSpecification());
    }

    @Description("#9 Проверка функциональности удаления юзера")
    @Test(priority = 9, groups = "deleteTest")
    public void deleteUserTest() {
        RestAssured
                .given()
                .spec(requestSpecification())
                .when()
                .delete("/user/{username}", "Apple")
                .then()
                .spec(responseSpecification());
    }

    @Description("№10 Негативный тест. Проверка вывода тела ошибки на запрос по несуществующему username и сверка ее полей (код, сообщение) с шаблонами")
    @Test(priority = 9, groups = "getTest")
    public void getUserByNameErrorTest() {
        UserError userError = RestAssured
                .given()
                .spec(requestSpecification())
                .when()
                .get("/user/{username}", "Apple")
                .then()
                .statusCode(404)
                .extract()
                .as(UserError.class);

        Assert.assertEquals(userError.getCode(), 1);
        Assert.assertEquals(userError.getMessage(), "User not found");
    }

}
