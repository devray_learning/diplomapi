package s100p.learning.diplom;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;

public class BaseTest {

    public static RequestSpecification requestSpecification() {

        return new RequestSpecBuilder()
                .log(LogDetail.ALL)
                .setBaseUri("https://petstore.swagger.io/v2")
                .setContentType(ContentType.JSON)
                .build();
    }

    public static RequestSpecification requestSpecificationWithAuthorization() {

        return new RequestSpecBuilder()
                .log(LogDetail.ALL)
                .setBaseUri("https://petstore.swagger.io/v2")
                .setContentType(ContentType.JSON)
                .addHeader("api_key", "special-key")
                .build();
    }

    public static ResponseSpecification responseSpecification() {

        return new ResponseSpecBuilder()
                .log(LogDetail.STATUS)
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200)
                .expectResponseTime(Matchers.lessThan(20000L))
                .build();
    }

    //в целом не рекомендуется делать тесты с параметрами и выносить их в базовые, чтобы потом вызывать через super.basicTest в дочернем классе
    public void basicTest(String basePath, String pathUrl, Object pathParam, int statusCode, String contentType) {
        RestAssured
                .given()
                .spec(requestSpecificationWithAuthorization())
                .basePath(basePath)
                .when()
                .get(pathUrl, pathParam)
                .then()
                .statusCode(statusCode)
                //это то же самое, что ниже, но короче
                .contentType(contentType);
                //.header("content-type", contentType);
    }

    //дополнительные перегрузки
    public void basicTest(String basePath, int statusCode, String contentType) {
        RestAssured
                .given()
                .spec(requestSpecificationWithAuthorization())
                .basePath(basePath)
                .when()
                .get()
                .then()
                .statusCode(statusCode)
                //это то же самое, что ниже, но короче
                .contentType(contentType);
                //.header("content-type", contentType);
    }

    public void basicTest(int statusCode, String contentType) {
        RestAssured
                .given()
                .spec(requestSpecificationWithAuthorization())
                .when()
                .get()
                .then()
                .statusCode(statusCode)
                //это то же самое, что ниже, но короче
                .contentType(contentType);
                //.header("content-type", contentType);
    }

}
