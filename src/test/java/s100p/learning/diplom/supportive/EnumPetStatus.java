package s100p.learning.diplom.supportive;

public enum EnumPetStatus {
    AVAILABLE("available"),
    SOLD("sold"),
    PENDING("pending");


    //Добавляем поле и конструктор, чтобы удобно хранить в константах нужное нам значение (может использоваться в некоторых методах DataGenerationUtility - через вызов значения поля name). Но я использовать не стал, т.к. ухудшает читаемость
    EnumPetStatus(String name) {
        this.name = name;
    }

    final String name;


}
