package s100p.learning.diplom.supportive;

import com.github.javafaker.Faker;
import s100p.learning.diplom.pogos.pet.Category;
import s100p.learning.diplom.pogos.pet.Pet;
import s100p.learning.diplom.pogos.pet.TagsItem;
import s100p.learning.diplom.pogos.user.User;

import java.util.List;
import java.util.Random;

public interface DataGenerationUtilities {

    Faker faker = new Faker();

    //базовый метод получения рандомного пета (по сути просто для инфо, в тестах не потребовался)
    default Pet getNewRandomPet() {
        List<String> photoUrls = List.of("https://fikiwiki.com/uploads/posts/2022-02/1644866304_45-fikiwiki-com-p-shchenki-krasivie-kartinki-49.jpg");
        int id = faker.number().numberBetween(0, 1000);
        List<TagsItem> tagsItemList = List.of(new TagsItem(faker.dog().gender(), id));
        Category category = new Category(faker.dog().breed(), id);
        List<String> statusList = List.of("available", "pending", "sold");
        String randomStatus = statusList.get(new Random().nextInt(statusList.size()));

        return new Pet(photoUrls, faker.dog().name(), id, category, tagsItemList, randomStatus);
    }

    //эти перегрузки нужны для целей других тестов, в которых нужно знать конкретный id или status пета
    default Pet getNewRandomPetWithId(int id) {
        List<String> photoUrls = List.of("https://fikiwiki.com/uploads/posts/2022-02/1644866304_45-fikiwiki-com-p-shchenki-krasivie-kartinki-49.jpg");
        List<TagsItem> tagsItemList = List.of(new TagsItem(faker.dog().gender(), id));
        Category category = new Category(faker.dog().breed(), id);
        List<String> statusList = List.of("available", "pending", "sold");
        //т.о. мы определяем случайность выбора значений
        String randomStatus = statusList.get(new Random().nextInt(statusList.size()));

        return new Pet(photoUrls, faker.dog().name(), id, category, tagsItemList, randomStatus);
    }


    default Pet getNewRandomPetWithStatus(String status) {
        List<String> photoUrls = List.of("https://fikiwiki.com/uploads/posts/2022-02/1644866304_45-fikiwiki-com-p-shchenki-krasivie-kartinki-49.jpg");
        int id = faker.number().numberBetween(0, 1000);
        List<TagsItem> tagsItemList = List.of(new TagsItem(faker.dog().gender(), id));
        Category category = new Category(faker.dog().breed(), id);

        return new Pet(photoUrls, faker.dog().name(), id, category, tagsItemList, status);
    }


    default User getNewRandomUser() {
        String firstName = faker.name().firstName();
        String lastName = faker.name().lastName();
        String password = faker.internet().password();
        int userStatus = faker.random().nextInt(0, 1);
        String phone = faker.phoneNumber().cellPhone();
        int id = faker.number().numberBetween(0, 1000);
        String email = faker.internet().emailAddress();
        String username = faker.name().username();


        return new User(firstName, lastName, password, userStatus, phone, id, email, username);
    }

    default User getNewRandomUserWithUserName(String username) {
        String firstName = faker.name().firstName();
        String lastName = faker.name().lastName();
        String password = faker.internet().password();
        int userStatus = faker.random().nextInt(0, 1);
        String phone = faker.phoneNumber().cellPhone();
        int id = faker.number().numberBetween(0, 1000);
        String email = faker.internet().emailAddress();

        return new User(firstName, lastName, password, userStatus, phone, id, email, username);
    }

    default User getNewRandomUserWithUserNameAndPassword(String username, String password) {
        String firstName = faker.name().firstName();
        String lastName = faker.name().lastName();
        int userStatus = faker.random().nextInt(0, 1);
        String phone = faker.phoneNumber().cellPhone();
        int id = faker.number().numberBetween(0, 1000);
        String email = faker.internet().emailAddress();

        return new User(firstName, lastName, password, userStatus, phone, id, email, username);
    }


}
