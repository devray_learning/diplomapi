package s100p.learning.diplom;

import io.qameta.allure.Description;
import io.restassured.RestAssured;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import s100p.learning.diplom.pogos.pet.Pet;
import s100p.learning.diplom.pogos.pet.PetError;
import s100p.learning.diplom.supportive.DataGenerationUtilities;
import s100p.learning.diplom.supportive.EnumPetStatus;

import java.io.File;

public class PetTest extends BaseTest implements DataGenerationUtilities {

    @DataProvider(name = "ID")
    public static Object[][] dataBB() {
        return new Object[][]{{2345}, {2344}};
    }

    @Description("Проверка функционала добавления нового пета")
    @Test(priority = 1, groups = "postTest", dataProvider = "ID")
    //@step используется для этапа теста, т.е. @test состоит из @step, не может быть метода с двумя аннотациями @test и @step
    public void addNewPetTest(int id) {

        RestAssured
                .given()
                .spec(requestSpecification())
                .body(getNewRandomPetWithId(id))
                .when()
                .post("/pet")
                .then()
                .spec(responseSpecification());
    }

    //тест такой же, но требуется для дальнейших тестов ниже, т.к. гарантирует наличие пета с указанным статусом
    @Description("Проверка функционала добавления нового пета с указанием статуса")
    @Test(priority = 2, groups = "postTest")
    public void addNewPetWithStatusTest() {

        RestAssured
                .given()
                .spec(requestSpecification())
                .body(getNewRandomPetWithStatus("available"))
                .when()
                .post("/pet")
                .then()
                .spec(responseSpecification());
    }

    @Description("Проверка базовых параметров: statusCode, contentTyp на запрос по id")
    @Test(priority = 3, groups = "basicTest", dataProvider = "ID")
    public void basicDataTest(int id) {
        super.basicTest("/pet", "https://petstore.swagger.io/v2/pet/{petId}", id, 200, "application/json");
    }

    @Description("Проверка получения тела объекта по заданному id")
    @Test(priority = 4, groups = "getTest", dataProvider = "ID")
    public void getPetByIdTest(int id) {
        RestAssured
                .given()
                .spec(requestSpecification())
                //.pathParam("petId","2345")
                .when()
                .get("/pet/{petId}", id)//при наличии path параметров запроса, в гет действии необходимо указать url содержащий фрейм path параметра ({petId}, его надо смотреть в самой апи и далее указать его значение, либо прописать в .pathParam() как указано выше
                .then()
                .spec(responseSpecification())
                .extract().as(Pet.class);
    }

    @Description("Проверка функционала добавления нового изображения для пета")
    @Test(priority = 5, groups = "postTest")
    public void addNewPetImageTest() {

        RestAssured
                .given()
                //т.к. эндпоинт апи содержит два поля formData в запросе, то 1) необходимо задать contentType, 2) разбить передачу fromData на две части (в два поля), через указание имени поля и значения для него
                .contentType("multipart/form-data")
                .multiPart("additionalMetadata", "when he was yong")
                .multiPart("file", new File("src\\test\\resources\\images\\doggie.jpeg"))
                .when()
                .post("https://petstore.swagger.io/v2/pet/{petId}/uploadImage", 2345)
                .then()
                .spec(responseSpecification());
    }

    @Description("Проверка функционала апдейта данных пета")
    @Test(priority = 6, groups = "putTest", dataProvider = "ID")
    public void upDatePetTest(int id) {

        RestAssured
                .given()
                .spec(requestSpecification())
                //поскольку я указываю тот же id при апдейте объекта, то фактически идет апдейт всех остальных полей, т.к. faker генерит для них новые значения
                .body(getNewRandomPetWithId(id))
                .when()
                .put("/pet")
                .then()
                .spec(responseSpecification());
    }

    @Description("Проверка функциональности поиска пета по статусу + сравнение значения поля с шаблоном")
    @Test(priority = 7, groups = "getTest")
    public void findPetByStatusTest() {
        String status = RestAssured
                .given()
                .spec(requestSpecification())
                .queryParam("status", EnumPetStatus.AVAILABLE.toString().toLowerCase(), EnumPetStatus.PENDING.toString().toLowerCase())
                .when()
                .get("/pet/findByStatus")
                .then()
                .spec(responseSpecification())
                .extract().jsonPath().get("[2].status");

        Assert.assertEquals(status, EnumPetStatus.AVAILABLE.toString().toLowerCase());
    }

    @Description("Негативный тест. Проверка базовых параметров: statusCode, contentType на запрос по несуществующему id")
    @Test(priority = 8, groups = "basicTest")
    public void basicDataErrorTest() {
        super.basicTest("/pet", "https://petstore.swagger.io/v2/pet/{petId}", 0, 404, "application/json");
    }

    @Description("Негативный тест. Проверка вывода тела ошибки на запрос по несуществующему id и сверка ее полей (тип, сообщение) с шаблонами")
    @Test(priority = 9, groups = "getTest")
    public void getPetByIdErrorTest() {
        PetError petError = RestAssured
                .given()
                .spec(requestSpecification())
                .when()
                .get("/pet/{petId}", 0)//при наличии path параметров запроса, в гет действии необходимо указать url содержащий фрейм path параметра ({petId}, его надо смотреть в самой апи и далее указать его значение, илбо прописать в .pathParam().
                .then()
                .statusCode(404)
                .extract()
                .as(PetError.class);

        Assert.assertEquals(petError.getType(), "error");
        Assert.assertEquals(petError.getMessage(), "Pet not found");

    }

    @Description("Проверка функциональности удаления пета")
    @Test(priority = 10, groups = "deleteTest", dataProvider = "ID")
    public void deletePetTest(int id) {
        RestAssured
                .given()
                .spec(requestSpecificationWithAuthorization())
                .when()
                .delete("/pet/{petId}", id)
                .then()
                .spec(responseSpecification());
    }

}
