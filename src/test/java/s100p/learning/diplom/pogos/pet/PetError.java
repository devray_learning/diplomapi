package s100p.learning.diplom.pogos.pet;

import lombok.Data;

@Data
public class PetError {
    private int code;
    private String type;
    private String message;
}