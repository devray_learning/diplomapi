package s100p.learning.diplom.pogos.user;

import lombok.Data;

import java.util.List;
@Data
public class UserList  {
	private List<User> userList;
}