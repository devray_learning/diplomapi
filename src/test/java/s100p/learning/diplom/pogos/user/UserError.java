package s100p.learning.diplom.pogos.user;

import lombok.Data;

@Data
public class UserError{
	private int code;
	private String type;
	private String message;
}
